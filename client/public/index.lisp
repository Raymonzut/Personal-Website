(html :lang 'en
  (head
    (meta :charset 'utf-8)
    (meta :name    'description
          :content "Raymon's personal website about his life and projects with a blog.")
    (meta :name    'viewport :content "width=device-width, initial-scale=1.0")
    (title "Raymon Zutekouw")

    (link :rel 'dns-prefetch :href "https://cdn.statically.io")

    (link :defer '
          :rel   'stylesheet
          :href  "https://cdn.statically.io/gh/dragonprojects/dragondesign/master/main.min.css"
          :media 'all)
    (link :defer '
          :rel   'stylesheet
          :href  "/css/general.css"
          :media 'all))
  (body
    (main
      (nav
        (a :href "/" "Home") "|"
        (a :href "/posts" "Posts") "|"
        (a :href "/qa" "QA"))
      (header
        (h1 "Home"))
      (p "Hi there, good to see you on my website.
          My name is Raymon Zutekouw; Self-taught programmer by heart.")
      (p "Building software and exploring the wide variety of tools (or making them) is my passion.
          To see it in action, checkout the stuff I make on "
        (a :href "https://github.com/Raymonzut" "GitHub."))

      (p "The projects that may be useful to others are open source; for inspiring others and improving each others work.
          That is why I am a huge fan of "
        (a :href "https://www.gnu.org/philosophy/free-sw.en.html" "free software")))))
