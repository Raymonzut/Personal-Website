(html :lang 'en
  (head
    (meta :charset 'utf-8)
    (meta :name    'description
          :content "Raymon answering common questions")
    (meta :name    'viewport :content "width=device-width, initial-scale=1.0")
    (title "Raymon Zutekouw")

    (link :rel 'dns-prefetch :href "https://cdn.statically.io")

    (link :defer '
          :rel   'stylesheet
          :href  "https://cdn.statically.io/gh/dragonprojects/dragondesign/master/main.min.css"
          :media 'all)
    (link :defer '
          :rel   'stylesheet
          :href  "/css/general.css"
          :media 'all))
  (body
    (main
      (nav
        (a :href "/" "Home") "|"
        (a :href "/posts" "Posts") "|"
        (a :href "/qa" "QA"))
      (article
        (header
         (h1 "Questions and answers"))
        (p :class "question" "What is this site about?")
        (p                   "This site is about me and the stuff I make")

        (p :class "question" "Why is it still so empty?")
        (p                   "Because I am framework hopping and now ditching frameworks all together")))))
