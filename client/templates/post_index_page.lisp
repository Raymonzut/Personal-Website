(html :lang 'en
  (head
    (meta :charset 'utf-8)
    (meta :name    'description
          :content "Raymon typing nonsense on his blog")
    (meta :name    'viewport :content "width=device-width, initial-scale=1.0")
    (title "Raymon Zutekouw")

    (link :rel 'dns-prefetch :href "https://cdn.statically.io")

    (link :defer '
          :rel   'stylesheet
          :href  "https://cdn.statically.io/gh/dragonprojects/dragondesign/master/main.min.css"
          :media 'all)
    (link :defer '
          :rel   'stylesheet
          :href  "/css/general.css"
          :media 'all))
  (body
    (main
      (nav
        (a :href "/" "Home") "|"
        (a :href "/posts" "Posts") "|"
        (a :href "/qa" "QA"))
      (header
        (h1 "Post listing"))
      "{{index}}")))
