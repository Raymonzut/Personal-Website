(html :lang 'en
  (head
    (meta :charset 'utf-8)
    (meta :name    'description
          :content "Raymon typing nonsense on his blog")
    (meta :name    'viewport :content "width=device-width, initial-scale=1.0")
    (title "{{title}} :: Raymon Zutekouw")

    (link :rel 'dns-prefetch :href "https://cdn.statically.io")

    (link :defer '
          :rel   'stylesheet
          :href  "https://cdn.statically.io/gh/dragonprojects/dragondesign/master/main.min.css"
          :media 'all)
    (link :defer '
          :rel   'stylesheet
          :href  "/css/general.css"
          :media 'all))
  (body
    (main
      (nav
        (a :href "/" "Home") "|"
        (a :href "/posts" "Posts") "|"
        (a :href "/qa" "QA"))
      (article
        (header
          (h1 "{{title}}")
          (p (strong "For those using a RSS reader, subscribe here: ")
             (a :href "/posts/rss.xml" "rss.xml")))
        (time :datetime "{{date}}")
        "{{content}}"))))
